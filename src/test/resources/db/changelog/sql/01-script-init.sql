DROP TABLE IF EXISTS transaction;
DROP TABLE IF EXISTS bank_account;
DROP TABLE IF EXISTS customer;
create table customer
(
    id             bigserial primary key,
    customer_id    varchar(255) not null,
    initial_credit numeric(19, 2),
    first_name     varchar(255) not null,
    last_name      varchar(255) not null,
    phone          varchar(255),
    unique (customer_id)
);
create table bank_account
(
    id             bigserial primary key,
    account_number integer        not null,
    balance        numeric(19, 2) not null,
    type           varchar(255),
    customer_id    bigint         not null references customer,
    unique (account_number)
);
create table transaction
(
    id         bigserial primary key,
    amount     numeric(19, 2) not null,
    date       timestamp,
    type       varchar(255),
    account_id bigint         not null references bank_account
);

