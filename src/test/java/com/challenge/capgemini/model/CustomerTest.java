package com.challenge.capgemini.model;

import com.challenge.capgemini.dto.request.CustomerRequestDto;
import com.challenge.capgemini.entity.AccountType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class CustomerTest {

    private static CustomerRequestDto initCustomerDto() {
        return CustomerRequestDto.
                builder().
                customerId("1000001").
                firstName("Alex").
                lastName("Michael").
                initialCredit(new BigDecimal(600)).
                phone("00989199636878").accountType(AccountType.CHECKING_ACCOUNT)
                .build();
    }

    @Test
    void shouldCreateCustomerRequestDto() {

        CustomerRequestDto requestDto = initCustomerDto();
        assertNotNull(requestDto.getCustomerId());
        assertEquals("1000001", requestDto.getCustomerId());
    }
}
