package com.challenge.capgemini.repository;

import com.challenge.capgemini.entity.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Sql(scripts = {"/db/changelog/sql/01-script-init.sql"})
@SpringBootTest
class CustomerRepositoryTest {
    @Autowired
    private CustomerRepository customerRepository;

    @Test
    void shouldCreateCustomer() {

        //given
        Customer customer = Customer.builder()
                .customerId("1000001")
                .firstName("Alex")
                .lastName("Michel")
                .initialCredit(new BigDecimal("200"))
                .phone("00989199636878")
                .build();
        customerRepository.save(customer);

        //when
        Optional<Customer> customerOptional = customerRepository.findByCustomerId(customer.getCustomerId());


        //assert
        assertFalse(customerOptional.isEmpty());
        assertEquals(customerOptional.get().getCustomerId(), customer.getCustomerId());
    }

}
