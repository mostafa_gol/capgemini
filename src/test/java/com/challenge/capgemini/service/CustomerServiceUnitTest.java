package com.challenge.capgemini.service;

import com.challenge.capgemini.dto.request.CustomerRequestDto;
import com.challenge.capgemini.entity.*;
import com.challenge.capgemini.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerServiceUnitTest {

    @InjectMocks
    CustomerServiceImpl customerService;

    @Mock
    AccountServiceImpl accountService;

    @Mock
    CustomerRepository customerRepository;

    @Mock
    TransactionServiceImpl transactionService;


    @Test
    void createAccount_saveAccount_returnNewAccount() {

        CustomerRequestDto customerRequestDto = new CustomerRequestDto();
        customerRequestDto.setCustomerId("10000001");
        customerRequestDto.setAccountType(AccountType.SAVING_ACCOUNT);
        customerRequestDto.setFirstName("Mostafa");
        customerRequestDto.setLastName("Golmohammadi");
        customerRequestDto.setPhone("00989199636878");
        customerRequestDto.setInitialCredit(new BigDecimal(600));

        Customer customer = Customer.builder()

                .customerId(customerRequestDto.getCustomerId())
                .firstName(customerRequestDto.getFirstName())
                .lastName(customerRequestDto.getLastName())
                .phone(customerRequestDto.getPhone())
                .initialCredit(customerRequestDto.getInitialCredit())
                .build();
        Customer customer2 = Customer.builder()
                .id(1L)
                .customerId(customerRequestDto.getCustomerId())
                .firstName(customerRequestDto.getFirstName())
                .lastName(customerRequestDto.getLastName())
                .phone(customerRequestDto.getPhone())
                .initialCredit(customerRequestDto.getInitialCredit())
                .build();


        BankAccount bankAccount = BankAccount.builder()
                .id(1L)
                .accountNumber(800111800)
                .balance(new BigDecimal(0))
                .customer(customer2)
                .type(AccountType.SAVING_ACCOUNT)
                .build();

        BankAccount bankAccount2 = BankAccount.builder()
                .id(1L)
                .accountNumber(800111800)
                .balance(bankAccount.getBalance().add(customerRequestDto.getInitialCredit()))
                .customer(customer2)
                .type(AccountType.SAVING_ACCOUNT)
                .build();

        Transaction transaction = Transaction.builder()
                .id(1L)
                .account(bankAccount2)
                .date(LocalDateTime.now())
                .amount(customerRequestDto.getInitialCredit())
                .type(TransactionType.DEPOSIT)
                .build();


        Optional<Customer> byCustomerId = Optional.empty();
        when(customerRepository.findByCustomerId(customer.getCustomerId())).thenReturn(byCustomerId);
        when(customerRepository.save(customer)).thenReturn(customer2);
        when(accountService.createAccount(customer2, customerRequestDto.getAccountType())).thenReturn(bankAccount);
        when(transactionService.createTransaction(bankAccount, customerRequestDto.getInitialCredit(),
                TransactionType.DEPOSIT)).thenReturn(transaction);

        BankAccount account = customerService.createAccount(customerRequestDto);

        assertEquals(customerRequestDto.getInitialCredit().intValue(), account.getBalance().intValue());
        assertEquals(customerRequestDto.getCustomerId(), account.getCustomer().getCustomerId());
        assertEquals(customerRequestDto.getAccountType(), account.getType());
        assertEquals(customerRequestDto.getFirstName(), account.getCustomer().getFirstName());
    }

}
