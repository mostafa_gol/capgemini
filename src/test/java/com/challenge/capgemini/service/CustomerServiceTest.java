package com.challenge.capgemini.service;

import com.challenge.capgemini.Application;
import com.challenge.capgemini.dto.request.CustomerRequestDto;
import com.challenge.capgemini.dto.request.TransactionRequestDto;
import com.challenge.capgemini.entity.AccountType;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Transaction;
import com.challenge.capgemini.entity.TransactionType;
import com.challenge.capgemini.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@Sql(scripts = {"/db/changelog/sql/01-script-init.sql"})
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CustomerServiceTest {
    @Autowired
    AccountService accountService;
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    CustomerService customerService;

    @Autowired
    TransactionService transactionService;

    @BeforeEach
    void deleteRecords() {
        accountRepository.deleteAll();
    }

    @Test
    void createAccount_saveAccount_returnNewAccount() {

        CustomerRequestDto customerRequestDto = new CustomerRequestDto();
        customerRequestDto.setCustomerId("10000001");
        customerRequestDto.setAccountType(AccountType.SAVING_ACCOUNT);
        customerRequestDto.setFirstName("Mostafa");
        customerRequestDto.setLastName("Golmohammadi");
        customerRequestDto.setPhone("00989199636878");
        customerRequestDto.setInitialCredit(new BigDecimal(600));

        BankAccount account = createAccount(customerRequestDto);

        assertEquals(account.getCustomer().getCustomerId(), customerRequestDto.getCustomerId());
    }


    BankAccount createAccount(CustomerRequestDto customerRequestDto) {
        return customerService.createAccount(customerRequestDto);
    }

    @Test
    void createTransaction_saveTransaction_returnCorrectBalance() {
        CustomerRequestDto customerRequestDto = new CustomerRequestDto();
        customerRequestDto.setCustomerId("10000001");
        customerRequestDto.setAccountType(AccountType.SAVING_ACCOUNT);
        customerRequestDto.setFirstName("Mostafa");
        customerRequestDto.setLastName("Golmohammadi");
        customerRequestDto.setPhone("00989199636878");
        customerRequestDto.setInitialCredit(new BigDecimal(600));
        BankAccount account = createAccount(customerRequestDto);

        TransactionRequestDto transactionRequestDto = new TransactionRequestDto();
        transactionRequestDto.setTransactionType(TransactionType.DEPOSIT);
        transactionRequestDto.setAmount(new BigDecimal(74000));
        transactionRequestDto.setAccountNumber(account.getAccountNumber());

        account = transactionService.createTransaction(transactionRequestDto);

        Transaction transaction = transactionService.createTransaction(account, new BigDecimal(2000), TransactionType.WITHDRAW);

        account = transaction.getAccount();


        assertEquals((600 + 74000 - 2000), account.getBalance().intValue());
    }

    @Test
    void createTransactions_saveTransactionAndUpdateBalance_returnCustomerTransactions() {
        CustomerRequestDto customerRequestDto = new CustomerRequestDto();
        customerRequestDto.setCustomerId("10000001");
        customerRequestDto.setAccountType(AccountType.SAVING_ACCOUNT);
        customerRequestDto.setFirstName("Mostafa");
        customerRequestDto.setLastName("Golmohammadi");
        customerRequestDto.setPhone("00989199636878");
        customerRequestDto.setInitialCredit(new BigDecimal(600));
        BankAccount account = createAccount(customerRequestDto);

        TransactionRequestDto transactionRequestDto = new TransactionRequestDto();
        transactionRequestDto.setTransactionType(TransactionType.DEPOSIT);
        transactionRequestDto.setAmount(new BigDecimal(74000));
        transactionRequestDto.setAccountNumber(account.getAccountNumber());

        account = transactionService.createTransaction(transactionRequestDto);

        Transaction transaction = transactionService.createTransaction(account, new BigDecimal(2000), TransactionType.WITHDRAW);

        account = transaction.getAccount();

        List<Transaction> transactions = customerService.getCustomerInfo(account.getAccountNumber());

        assertEquals(3, transactions.size());
        assertEquals(transactions.get(0).getAmount().intValue(), new BigDecimal(600).intValue());
        assertEquals(TransactionType.DEPOSIT, transactions.get(0).getType());
        assertEquals(transactions.get(1).getAmount().intValue(), new BigDecimal(74000).intValue());
        assertEquals(TransactionType.DEPOSIT, transactions.get(1).getType());
        assertEquals(transactions.get(2).getAmount().intValue(), new BigDecimal(2000).intValue());
        assertEquals(TransactionType.WITHDRAW, transactions.get(2).getType());

    }

}
