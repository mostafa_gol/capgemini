package com.challenge.capgemini.entity;

public enum AccountType {
    CHECKING_ACCOUNT,
    SAVING_ACCOUNT
}
