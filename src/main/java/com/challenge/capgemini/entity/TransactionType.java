package com.challenge.capgemini.entity;

public enum TransactionType {
    DEPOSIT,
    WITHDRAW
}
