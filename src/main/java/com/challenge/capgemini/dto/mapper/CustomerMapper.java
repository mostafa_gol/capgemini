package com.challenge.capgemini.dto.mapper;

import com.challenge.capgemini.dto.response.CustomerResponseDto;
import com.challenge.capgemini.entity.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<CustomerResponseDto, Customer> {
}
