package com.challenge.capgemini.dto.mapper;

import com.challenge.capgemini.dto.response.TransactionResponseDto;
import com.challenge.capgemini.dto.response.TransactionsDto;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Transaction;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionMapper extends EntityMapper<TransactionResponseDto, Transaction> {

    default TransactionResponseDto toResponseDto(List<Transaction> entityList) {
        if (entityList == null) {
            return null;
        }
        BankAccount account = entityList.get(0).getAccount();
        List<TransactionsDto> transactionsInfoDtos = entityToTransactionsInfoDtoList(entityList);
        TransactionResponseDto transactionResponseDto = new TransactionResponseDto();
        transactionResponseDto.setTransactions(transactionsInfoDtos);
        transactionResponseDto.setFirstName(account.getCustomer().getFirstName());
        transactionResponseDto.setLastName(account.getCustomer().getLastName());
        transactionResponseDto.setAccountNumber(account.getAccountNumber());
        transactionResponseDto.setCustomerId(account.getCustomer().getCustomerId());
        transactionResponseDto.setBalance(account.getBalance());
        return transactionResponseDto;
    }

    default List<TransactionsDto> entityToTransactionsInfoDtoList(List<Transaction> entityList) {
        if (entityList == null) {
            return Collections.emptyList();
        }
        List<TransactionsDto> list = new ArrayList<>(entityList.size());
        for (Transaction transaction : entityList) {
            list.add(entityToTransactionsInfoDto(transaction));
        }
        return list;
    }

    default TransactionsDto entityToTransactionsInfoDto(Transaction entity) {
        if (entity == null) {
            return null;
        }
        TransactionsDto transactionsInfoDto = new TransactionsDto();
        transactionsInfoDto.setDate(entity.getDate());
        transactionsInfoDto.setAmount(entity.getAmount());
        transactionsInfoDto.setType(entity.getType());
        return transactionsInfoDto;
    }
}
