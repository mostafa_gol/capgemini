package com.challenge.capgemini.dto.mapper;

import com.challenge.capgemini.dto.response.AccountResponseDto;
import com.challenge.capgemini.entity.BankAccount;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper extends EntityMapper<AccountResponseDto, BankAccount> {
}
