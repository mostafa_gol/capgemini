package com.challenge.capgemini.dto.request;

import com.challenge.capgemini.entity.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequestDto {
    @NotNull
    private Integer accountNumber;
    @NotNull
    private BigDecimal amount = new BigDecimal(0);

    @NotNull
    private TransactionType transactionType = TransactionType.DEPOSIT;
}
