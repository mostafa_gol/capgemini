package com.challenge.capgemini.dto.request;

import com.challenge.capgemini.entity.AccountType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequestDto {

    @Pattern(regexp = "\\d+", message = "customer-id should be numeric")
    @NotNull
    private String customerId;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    private String phone;
    @NotNull
    private AccountType accountType;

    @Min(value = 0, message = "initial credit can not be less than zero")
    private BigDecimal initialCredit = new BigDecimal(0);
}
