package com.challenge.capgemini.dto.response;

import com.challenge.capgemini.entity.TransactionType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionsDto {
    private LocalDateTime date;
    private BigDecimal amount;
    private TransactionType type;
}
