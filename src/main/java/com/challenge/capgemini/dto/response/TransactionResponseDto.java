package com.challenge.capgemini.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionResponseDto {

    List<TransactionsDto> transactions;
    private BigDecimal balance;
    private Integer accountNumber;
    private String customerId;
    private String firstName;
    private String lastName;
}
