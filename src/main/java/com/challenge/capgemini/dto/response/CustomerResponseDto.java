package com.challenge.capgemini.dto.response;

import com.challenge.capgemini.entity.AccountType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerResponseDto {
    private String customerId;
    private String accountNumber;
    private String firstName;
    private String lastName;
    private String phone;
    private AccountType accountType;
}
