package com.challenge.capgemini.dto.response;

import com.challenge.capgemini.entity.AccountType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountResponseDto {
    private Integer accountNumber;
    private String balance;
    private CustomerResponseDto customer;
    private AccountType type;
}
