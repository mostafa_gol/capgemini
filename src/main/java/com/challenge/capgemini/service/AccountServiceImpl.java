package com.challenge.capgemini.service;

import com.challenge.capgemini.entity.AccountType;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Customer;
import com.challenge.capgemini.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Random;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {
    private static final Random RANDOM = new Random();
    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    @Transactional
    public BankAccount createAccount(Customer customer, AccountType accountType) {
        log.info("Request to create account for customer-id :{} and account type:{}", customer.getCustomerId(), accountType);
        BankAccount bankAccount = BankAccount.builder()
                .accountNumber(RANDOM.nextInt(800111800))
                .balance(new BigDecimal(0))
                .customer(customer)
                .type(accountType)
                .build();
        return accountRepository.save(bankAccount);
    }

    @Override
    public Optional<BankAccount> getAccount(Integer accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }

}
