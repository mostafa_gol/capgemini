package com.challenge.capgemini.service;


import com.challenge.capgemini.dto.request.CustomerRequestDto;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Customer;
import com.challenge.capgemini.entity.Transaction;
import com.challenge.capgemini.entity.TransactionType;
import com.challenge.capgemini.exception.CustomException;
import com.challenge.capgemini.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;
    private final AccountService accountService;
    private final TransactionService transactionService;


    public CustomerServiceImpl(CustomerRepository customerRepository, AccountService accountService,
                               TransactionService transactionService) {
        this.customerRepository = customerRepository;
        this.accountService = accountService;
        this.transactionService = transactionService;

    }

    @Transactional
    @Override
    public BankAccount createAccount(CustomerRequestDto customerRequestDto) {
        Customer customer;
        Optional<Customer> optionalCustomer = customerRepository.findByCustomerId(customerRequestDto.getCustomerId());
        customer = optionalCustomer.orElseGet(Customer::new);

        customer.setCustomerId(customerRequestDto.getCustomerId());
        customer.setFirstName(customerRequestDto.getFirstName());
        customer.setLastName(customerRequestDto.getLastName());
        customer.setPhone(customerRequestDto.getPhone());
        customer.setInitialCredit(customerRequestDto.getInitialCredit());


        Customer savedCustomer = customerRepository.save(customer);


        BankAccount account = accountService.createAccount(savedCustomer, customerRequestDto.getAccountType());

        if (Objects.isNull(customerRequestDto.getInitialCredit()) ||
                (customerRequestDto.getInitialCredit().compareTo(new BigDecimal(0)) == 0)) {
            log.info("account created successfully for customer-id: {}", savedCustomer.getCustomerId());
            return account;
        }
        Transaction transaction = transactionService.createTransaction(account,
                customerRequestDto.getInitialCredit(),
                TransactionType.DEPOSIT);
        log.info("account created successfully for customer-id: {}", savedCustomer.getCustomerId());
        return transaction.getAccount();
    }

    @Override
    @Transactional
    public List<Transaction> getCustomerInfo(Integer accountNumber) {
        BankAccount account = accountService.getAccount(accountNumber)
                .orElseThrow(() ->
                        new CustomException("Account with acc-number: " + accountNumber + " not found", HttpStatus.NOT_FOUND));
        return transactionService.getAllTransactions(Collections.singletonList(account));
    }
}




