package com.challenge.capgemini.service;

import com.challenge.capgemini.entity.AccountType;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Customer;

import java.util.Optional;

public interface AccountService {

    BankAccount createAccount(Customer customer, AccountType accountType);

    Optional<BankAccount> getAccount(Integer accountNumber);
}
