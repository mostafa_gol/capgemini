package com.challenge.capgemini.service;

import com.challenge.capgemini.dto.request.TransactionRequestDto;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Transaction;
import com.challenge.capgemini.entity.TransactionType;
import com.challenge.capgemini.exception.CustomException;
import com.challenge.capgemini.repository.AccountRepository;
import com.challenge.capgemini.repository.TransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;

    public TransactionServiceImpl(TransactionRepository transactionRepository,
                                  AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public Transaction createTransaction(BankAccount account, BigDecimal amount, TransactionType transactionType) {
        log.info("Request to create transaction for account-number: {} with amount: {} and type: {}", account.getAccountNumber(), account, transactionType);
        BigDecimal balance;
        if (TransactionType.DEPOSIT.equals(transactionType)) {
            balance = account.getBalance().add(amount);
        } else {
            balance = account.getBalance().subtract(amount);
        }

        if (balance.compareTo(new BigDecimal(0)) <= 0) {
            throw new CustomException("non-sufficient funds", HttpStatus.METHOD_NOT_ALLOWED);
        }
        Transaction transaction = Transaction.builder()
                .account(account)
                .date(LocalDateTime.now())
                .amount(amount)
                .type(transactionType)
                .build();
        transaction = transactionRepository.save(transaction);
        account.setBalance(balance);
        accountRepository.save(account);
        log.info("balance account updated successfully");
        return transaction;
    }

    @Override
    public List<Transaction> getAllTransactions(List<BankAccount> bankAccounts) {
        return transactionRepository.findAllByAccountIn(bankAccounts);
    }

    @Override
    public BankAccount createTransaction(TransactionRequestDto transactionRequestDto) {
        Transaction transaction;
        Optional<BankAccount> optionalAccount = accountRepository.findByAccountNumber(transactionRequestDto.getAccountNumber());
        BankAccount bankAccount = optionalAccount.orElseThrow(() -> new CustomException("Account number: " + transactionRequestDto.getAccountNumber() + " not found", HttpStatus.NOT_FOUND));
        if (TransactionType.DEPOSIT.equals(transactionRequestDto.getTransactionType())) {
            transaction = createTransaction(bankAccount, transactionRequestDto.getAmount(), TransactionType.DEPOSIT);
        } else {
            transaction = createTransaction(bankAccount, transactionRequestDto.getAmount(), TransactionType.WITHDRAW);
        }
        return transaction.getAccount();
    }

}
