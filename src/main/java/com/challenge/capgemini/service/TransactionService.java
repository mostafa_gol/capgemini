package com.challenge.capgemini.service;

import com.challenge.capgemini.dto.request.TransactionRequestDto;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Transaction;
import com.challenge.capgemini.entity.TransactionType;

import java.math.BigDecimal;
import java.util.List;

public interface TransactionService {

    Transaction createTransaction(BankAccount account, BigDecimal amount, TransactionType transactionType);

    List<Transaction> getAllTransactions(List<BankAccount> bankAccounts);

    BankAccount createTransaction(TransactionRequestDto transactionRequestDto);
}
