package com.challenge.capgemini.service;


import com.challenge.capgemini.dto.request.CustomerRequestDto;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Transaction;

import java.util.List;

public interface CustomerService {
    BankAccount createAccount(CustomerRequestDto customerRequestDto);

    List<Transaction> getCustomerInfo(Integer accountNumber);
}
