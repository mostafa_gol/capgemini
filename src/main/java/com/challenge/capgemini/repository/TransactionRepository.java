package com.challenge.capgemini.repository;

import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>, JpaSpecificationExecutor<Transaction> {

    List<Transaction> findAllByAccountIn(List<BankAccount> accounts);
}
