package com.challenge.capgemini.rest;

import com.challenge.capgemini.dto.mapper.AccountMapper;
import com.challenge.capgemini.dto.request.TransactionRequestDto;
import com.challenge.capgemini.dto.response.AccountResponseDto;
import com.challenge.capgemini.service.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class TransactionResource {
    private final TransactionService transactionService;
    private final AccountMapper accountMapper;

    public TransactionResource(TransactionService transactionService, AccountMapper accountMapper) {
        this.transactionService = transactionService;
        this.accountMapper = accountMapper;
    }

    @PostMapping("/transactions")
    public ResponseEntity<AccountResponseDto> createTransaction(@RequestBody @Valid TransactionRequestDto transactionRequestDto) {
        return ResponseEntity.ok(accountMapper.toDto(transactionService.createTransaction(transactionRequestDto)));
    }

}
