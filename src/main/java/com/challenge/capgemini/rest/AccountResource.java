package com.challenge.capgemini.rest;

import com.challenge.capgemini.dto.mapper.CustomerMapper;
import com.challenge.capgemini.dto.request.CustomerRequestDto;
import com.challenge.capgemini.dto.response.CustomerResponseDto;
import com.challenge.capgemini.entity.BankAccount;
import com.challenge.capgemini.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class AccountResource {

    private final CustomerService customerService;
    private final CustomerMapper customerMapper;

    public AccountResource(CustomerService customerService, CustomerMapper customerMapper) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
    }

    @PostMapping("/accounts")
    public ResponseEntity<CustomerResponseDto> createAccount(@RequestBody @Valid CustomerRequestDto customerRequestDto) {
        BankAccount account = customerService.createAccount(customerRequestDto);
        return ResponseEntity.ok(customerMapper.toDto(account.getCustomer()));
    }
}
