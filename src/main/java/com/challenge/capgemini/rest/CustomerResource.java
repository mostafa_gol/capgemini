package com.challenge.capgemini.rest;

import com.challenge.capgemini.dto.mapper.TransactionMapper;
import com.challenge.capgemini.dto.response.TransactionResponseDto;
import com.challenge.capgemini.entity.Transaction;
import com.challenge.capgemini.service.CustomerService;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CustomerResource {

    private final CustomerService customerService;
    private final TransactionMapper transactionMapper;

    public CustomerResource(CustomerService customerService,
                            TransactionMapper transactionMapper) {
        this.customerService = customerService;
        this.transactionMapper = transactionMapper;
    }

    @GetMapping("/customers/accounts/{accountNumber}")
    public ResponseEntity<TransactionResponseDto> gatCustomerInfo(@ApiParam(value = "account number id related to the account of customer",
            required = true) @PathVariable Integer accountNumber) {
        List<Transaction> transactions = customerService.getCustomerInfo(accountNumber);
        return ResponseEntity.ok(transactionMapper.toResponseDto(transactions));
    }
}
