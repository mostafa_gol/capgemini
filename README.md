for running the application use these command: </br>

1- `mvn clean package`

2- `docker-compose -f docker-compose.yaml up --build`

The application use: <br>

spring boot 2.5.6 <br>
maven <br>
postgresql database for dev profile and h2 for testing <br>

Swagger address: http://localhost:8080/swagger-ui.html# <br>

You can see all APIs : `http://localhost:8080/swagger-ui.html#/` <br>

1- `first create an account and customer` : http://localhost:8080/api/v1/accounts <br> 
2- `use account-number related to the account of customer` <br>
`find accountNamber of customer in DB for calling customer info`<br>
http://localhost:8080/api/v1/customers/accounts/{accountNamber} <br>



3- `for creation an transaction use this api:` <br>
http://localhost:8080/api/v1/transactions